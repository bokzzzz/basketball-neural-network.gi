import numpy as np
from math import sqrt, tan, cos
import time


@np.vectorize
def sigmoid(x):
    return 1 / (1 + np.e ** -x)


def shot_speed(x):
    return ((112155 * x) / sqrt(1282225820 * x - 705362669) - 6) / 12


class SimpleNN:
    def __init__(self, layer_dimensions, learning_rate):
        self.input_dim = layer_dimensions[0]
        self.hidden_dim = layer_dimensions[1]
        self.output_dim = layer_dimensions[2]
        self.learning_rate = learning_rate

        self.weights_in_hidden = np.random.uniform(
            -0.2, 0.2, (self.input_dim) * self.hidden_dim)

        self.weights_hidden_out = np.random.uniform(
            -0.2, 0.2, self.output_dim * (self.hidden_dim))

        self.weights_in_hidden = self.weights_in_hidden.reshape(
            self.hidden_dim, self.input_dim)

        self.weights_hidden_out = self.weights_hidden_out.reshape(
            self.output_dim, self.hidden_dim)

    def train(self, input_vector, target_vector):
        # transponovanje uloznog niza da bi se moglo pomnoziti sa matricom
        input_vector = np.array(input_vector, ndmin=2).T
        target_vector = np.array(target_vector, ndmin=2).T
        # mnozenje ulaznih tezina sa ulazom
        hidden_layer_out = sigmoid(
            np.dot(self.weights_in_hidden, input_vector))
        # mnozenje izlaznih tezina sa izlaznim vrijednostima skrivenog sloja
        out = sigmoid(np.dot(self.weights_hidden_out, hidden_layer_out))
        # proracun greske izlaza
        output_errors = target_vector - out
        # izracunavanje delta faktora
        error_gradients = output_errors * out * (1.0 - out)
        # izmjenja tezina po granama
        # tezinski faktor * deltra faktor *izlaz iz skrivenog sloja
        h_o_updates = self.learning_rate * \
                      np.dot(error_gradients, hidden_layer_out.T)
        # stari tezinski faktor + korekcija je novi tezinski faktor
        self.weights_hidden_out += h_o_updates
        # mjenja se tezine izmedju ulaznog i skrivenog
        #
        # hidden_errors = np.dot(self.weights_hidden_out.T, output_errors)
        hidden_errors = np.dot(self.weights_hidden_out.T, error_gradients)
        error_gradients = hidden_errors * hidden_layer_out * \
                          (1.0 - hidden_layer_out)
        i_h_updates = np.dot(error_gradients, input_vector.T)
        #
        self.weights_in_hidden += self.learning_rate * i_h_updates

    def run(self, input):
        # propustim kroz mrezu
        input = np.array(input).T
        hidden_layer_out = sigmoid(np.dot(self.weights_in_hidden, input))
        return sigmoid(np.dot(self.weights_hidden_out, hidden_layer_out))


def get_dataset(size):
    x = np.random.uniform(low=6.5, high=18.25, size=size)
    v_calc = np.vectorize(shot_speed)
    v = v_calc(x)
    return x, v


if __name__ == "__main__":
    training_set_size = 2000
    validation_set_size = 2000
    epoch_count = 100
    startTime = time.time()
    x, v = get_dataset(training_set_size)
    nn = SimpleNN([1, 10, 1], learning_rate=0.02)

    for i in range(epoch_count):
        print("epoha", i)
        for j in range(len(x)):
            nn.train(x[j], v[j])

    val_x, expected = get_dataset(validation_set_size)
    correct_count = 0

    for i in range(len(expected)):
        res = nn.run(val_x[i])
        correct_count += int((abs(expected[i] - res) < 0.0047))

    print("Accuracy: {}%".format(
        (correct_count / float(validation_set_size)) * 100))
    endTime = time.time() - startTime
    print("Time: {} seconds".format(endTime))
    while True:
        inputFirst = float(input("Unesite udaljenost od kosa: "))
        speed = nn.run(inputFirst) * 12 + 6
        print("Visina kod bloka na 1m", 2.5 + tan(0.7853) - (9.81 / 2) / (speed ** 2 * cos(0.7853) ** 2))
        print("Visina kod kosa na pocetku kosa ",
              2.5 + (inputFirst - 0.1) * tan(0.7853) - (9.81 / 2) * (inputFirst - 0.1) ** 2 / (
                      speed ** 2 * cos(0.7853) ** 2))
        print("Visina kod centra kod kosa",
              2.5 + inputFirst * tan(0.7853) - (9.81 / 2) * inputFirst ** 2 / (speed ** 2 * cos(0.7853) ** 2))
        print("Visina kod kraj kosa", 2.5 + (inputFirst + 0.1) * tan(0.7853) - (9.81 / 2) * (inputFirst + 0.1) ** 2 / (
                speed ** 2 * cos(0.7853) ** 2))
